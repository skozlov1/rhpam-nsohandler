package org.optus.asimov;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jbpm.process.workitem.rest.RESTWorkItemHandler;

public class GarJwtRestWIH extends RESTWorkItemHandler {
  
  @Override
  protected String transformRequest(Object data, String contentType) {
    try {
      if ( contentType.toLowerCase().contains("application/json")
              || contentType.toLowerCase().contains("application/yang-data+json")) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(data);
      }

    } catch (Exception var5) {
      throw new RuntimeException("Unable to transform request to object", var5);
    }

    throw new IllegalArgumentException("Unable to find transformer for content type '" + contentType + "' to handle data " + data);
  }
}
